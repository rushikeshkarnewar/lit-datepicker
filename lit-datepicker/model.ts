export interface CalendarItem {
  date: any;
  disable: boolean;
  // current: boolean;
  between: boolean;
  selected: boolean;
}
