/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable prefer-const */
import moment from 'moment';
import { CalendarItem } from './model';

export function buildCalendar(
  month: any,
  year: any,
  selectedDate?: any,
  rangeDates?: { startDate: any; endDate: any },
) {
  let daysInMonth: any;
  const start = moment()
    .year(year)
    .startOf('year');
  const firstDayOfMonth = parseInt(
    moment()
      .date(1)
      .month(month)
      .year(year)
      .format('d'),
    10,
  );
  const days: CalendarItem[] = [];

  /*
    Adding blank space in array to adjust date according to weekday
    */
  if (firstDayOfMonth !== 0) {
    for (let i = 0; i < firstDayOfMonth; i += 1) {
      days.push({
        date: ' ',
        disable: true,
        between: false,
        selected: false,
      });
    }
  }

  daysInMonth = moment()
    .month(month)
    .year(year)
    .daysInMonth();
  const currentDate = moment().format('YYYY-MM-DD');
  for (let j = 1; j <= daysInMonth; j += 1) {
    const iterableDate = start
      .date(j)
      .month(month)
      .year(year)
      .format('YYYY-MM-DD');

    let isSelectDate = false;
    let isDisable = false;
    let isBetween = false;

    moment(iterableDate).isBefore(currentDate)
    ? (isDisable = true)
    : null;

    moment(iterableDate).isAfter(moment().add(365, 'd'))
    ? (isDisable = true)
    : null;

    if (selectedDate) {
      const currentSelectedDate = selectedDate
        ? selectedDate
        : moment().format('YYYY-MM-DD');

      moment(iterableDate).isSame(currentSelectedDate)
        ? (isSelectDate = true)
        : null;
    } else {
      const currentSelectedStartDate = rangeDates!.startDate
        ? rangeDates!.startDate
        : moment().format('YYYY-MM-DD');
      const currentSelectedEndDate = rangeDates!.endDate
        ? rangeDates!.endDate
        : null;

      if (currentSelectedStartDate && currentSelectedEndDate) {
        moment(iterableDate).isBetween(
          currentSelectedStartDate,
          currentSelectedEndDate,
        )
          ? (isBetween = true)
          : null;
      }

      moment(iterableDate).isSame(currentSelectedStartDate)
        ? (isSelectDate = true)
        : null;

      moment(iterableDate).isSame(currentSelectedEndDate)
        ? (isSelectDate = true)
        : null;
    }

    days.push({
      date: moment(iterableDate).month(month).year(year),
      disable: isDisable,
      selected: isSelectDate,
      between: isBetween,
    });
  }
  return days;
}
