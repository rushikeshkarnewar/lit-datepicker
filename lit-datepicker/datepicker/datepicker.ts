import { html, customElement, LitElement } from 'lit-element';
// import { DatePicker } from 'carbon-components';
import datepickerScss from '../datepicker.scss';
import { CalendarItem } from '../model';
import moment from 'moment';
import { classMap } from 'lit-html/directives/class-map';
// import * as calendarService from './service';
// import { uniqueID } from '../../util';

@customElement('custom-datepicker')
export default class OrxeCustomDatePicker extends LitElement {
  days: Array<CalendarItem> = [];
  daysList: Array<any> = [];
  date: Date = new Date();
  monthName = '';
  selectedStartDate = this.date.toISOString().slice(0, 10);
  previousStartDate;
  showCalendar = false;

  static get styles() {
    return datepickerScss;
  }

  connectedCallback() {
    super.connectedCallback();
  }

  getDays() {
    this.daysList = [];
    if (this.days[0].date.day() >= 1) {
      for (let i = 0; i < this.days[0].date.day(); i++) {
        this.daysList.push(html`
          <li tabindex="-1" role="button">
            ${this.getDateLabel(' ')}
          </li>
        `);
      }
    }
    let ariaLabelFormat;
    this.days.map((day) => {
      ariaLabelFormat = moment(day.date, 'MM-DD-YY');
      this.daysList.push(
        html`
          <li
            tabindex="-1"
            role="button"
            id=${moment(ariaLabelFormat).format('YYYYMMDD')}
            aria-label=${moment(ariaLabelFormat).format('DD MMMM YYYY, dddd')}
            @click=${(e) => {
              day.selected = true;
              this.selectDate(day, e);
            }}
            class=${classMap({
              current: day.date.date() === this.date.getDate(),
              selected: day.selected,
            })}
          >
            ${this.getDateLabel(day.date)}
            ${moment(day.date).date() == this.date.getDate() &&
            moment(day.date).month() == this.date.getMonth() &&
            moment(day.date).year() == this.date.getFullYear()
              ? html`
                  <div class="current-date"></div>
                `
              : ''}
          </li>
        `,
      );
    });
    this.requestUpdate();
  }

  firstUpdated() {
    this.buildCalendar(this.date);
    this.getDays();
  }

  buildCalendar(date) {
    const month = date.getMonth();
    const year = date.getFullYear();
    const daysInMonth = moment()
      .month(month)
      .year(year)
      .daysInMonth();
    this.days = [];
    for (let i = 1; i <= daysInMonth; i++) {
      this.days.push({
        date: moment()
          .year(year)
          .startOf('year')
          .date(i)
          .month(month)
          .year(year),
        disable: false,
        selected: false,
        between: false,
      });
    }
    this.getDays();
    this.monthName = this.getMonthName(date);
  }

  selectDate(item, elem) {
    if (item.selected) elem.target.classList.add('selected');
    if (item.disable) return;
    if (!this.previousStartDate) {
        this.previousStartDate = elem.target;
    }
    if (
      this.previousStartDate.innerHTML !== moment(item.date).format('DD')
    ) {
      this.previousStartDate.classList.remove('selected');
      this.previousStartDate = elem.target;
      if (item.selected) elem.target.classList.add('selected');
    }
    this.selectedStartDate = moment(item.date).format('YYYY-MM-DD');
    this.showCalendar = false;
    this.requestUpdate();
  }

  getDateLabel(item) {
    if (item === ' ') {
      return ' ';
    }
    return moment(item).format('DD');
  }

  getMonthName(date) {
    const monthName = new Intl.DateTimeFormat('en-US', { month: 'long' }).format;
    const longName = monthName(date);
    return longName;
  }

  _toggleCalendar() {
    this.showCalendar = !this.showCalendar;
    this.requestUpdate();
  }

  render() {
    const currentDate = this.date;
    return html`
      <div
        class="datepicker-wrapper"
        @click=${this._toggleCalendar}
        @blur=${() => {
          this.showCalendar = false;
          this.requestUpdate();
        }}
      >
        <input
          type="text"
          class="input-date"
          placeholder="dd/mm/yyyy"
          .value=${this.selectedStartDate}
        />
        <svg
          focusable="false"
          preserveAspectRatio="xMidYMid meet"
          style="will-change: transform;"
          xmlns="http://www.w3.org/2000/svg"
          data-date-picker-icon="true"
          class="bx--date-picker__icon"
          width="20"
          height="24"
          viewBox="0 0 14 14"
          aria-hidden="true"
        >
          <path
            d="M13,2h-2V1h-1v1H6V1H5v1H3C2.4,2,2,2.4,2,3v10c0,0.6,0.4,1,1,1h10c0.6,0,1-0.4,1-1V3C14,2.4,13.6,2,13,2z M13,13H3V6h10V13z M13,5H3V3h2v1h1V3h4v1h1V3h2V5z"
          ></path>
        </svg>
      </div>
      <div
        class=${classMap({
          'hide-calendar': !this.showCalendar,
          'calendar': true,
        })}
      >
        <ul class="weekdays">
          <li>S</li>
          <li>M</li>
          <li>T</li>
          <li>W</li>
          <li>T</li>
          <li>F</li>
          <li>S</li>
        </ul>
        <div style="display:flex;justify-content:center;padding: 0 5px 0 5px">
          <span
            @click=${() => {
              currentDate.setMonth(currentDate.getMonth() - 1);
              if (currentDate.getMonth() < 0) {
                currentDate.setFullYear(currentDate.getFullYear() - 1);
              }
              this.buildCalendar(currentDate);
            }}
            style="flex:auto;cursor:pointer"
            >&#10094;</span
          >
          <span style="flex:auto">${this.monthName} ${this.date.getFullYear()}</span>
          <span
            @click=${() => {
              currentDate.setMonth(currentDate.getMonth() + 1);
              if (currentDate.getMonth() > 12) {
                currentDate.setFullYear(currentDate.getFullYear() + 1);
              }
              this.buildCalendar(currentDate);
            }}
            style="cursor:pointer"
            >&#10095;</span
          >
        </div>
        <ul class="days" role="group">
          ${this.daysList}
        </ul>
      </div>
    `;
  }
}
