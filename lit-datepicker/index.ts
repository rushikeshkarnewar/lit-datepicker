import { html, customElement, LitElement, property } from 'lit-element';
// import { DatePicker } from 'carbon-components';
// import datepickerScss from './datepicker.scss';
// import { CalendarItem } from './model';
import moment from 'moment';
// import { classMap } from 'lit-html/directives/class-map';
// import * as calendarService from './service';
// import { uniqueID } from '../../util';
import './datepicker/datepicker';
import './range-datepicker/range-datepicker';

@customElement('orxe-datepicker')
export default class OrxeDatePicker extends LitElement {
  @property({ type: String }) datepickerMode: 'single' | 'range' = 'single';
  @property({ type: String }) inputType: 'text-readonly' | 'icon' | 'text' = 'text-readonly';
  @property({ type: String }) dateFormat = 'DD-MM-YYYY';

  @property({ type: String }) selectedStartDate? = moment().format('YYYY-MM-DD');
  @property({ type: String }) selectedEndDate? = null;
  @property({ type: String }) selectedSingleDate? = moment().format('YYYY-MM-DD');

  renderTemplate() {
    return this.datepickerMode === 'single'
      ? html`
          <custom-datepicker></custom-datepicker>
        `
      : html`
          <range-datepicker></range-datepicker>
        `;
  }

  render() {
    return this.renderTemplate();
  }
}
