/* eslint-disable no-console */
import { html, customElement, LitElement } from 'lit-element';
// import { DatePicker } from 'carbon-components';
import datepickerScss from '../datepicker.scss';
import { CalendarItem } from '../model';
import * as service from '../service';
import moment from 'moment';
import { classMap } from 'lit-html/directives/class-map';
import '../datepicker/datepicker';

@customElement('range-datepicker')
export default class OrxeRangeDatePicker extends LitElement {
  days: Array<CalendarItem> = [];
  currentDaysList: Array<any> = [];
  date: Date = new Date();
  selectedStartDate = this.date.toISOString().slice(0, 10);
  selectedEndDate = this.date.toISOString().slice(0, 10);
  startDateSelected = false;
  endDateSelected = false;
  previousStartDate;
  previousEndDate;
  start;
  end;
  showCalendar = false;
  nextDate = new Date();
  nextMonthDaysList;
  nextEndDate;
  nextMonthStartDate;
  nextMonthEndDate;
  nextMonthDays: Array<CalendarItem> = [];

  static get styles() {
    return datepickerScss;
  }
  getDays(daysInMonth) {
    const daysList: Array<any> = [];
    let ariaLabelFormat;
    daysInMonth.map((day) => {
      ariaLabelFormat = moment(day.date, 'MM-DD-YY');
      daysList.push(
        html`
          <li
            tabindex="-1"
            role="button"
            id=${moment(ariaLabelFormat).format('YYYYMMDD')}
            aria-label=${moment(ariaLabelFormat).format('DD MMMM YYYY, dddd')}
            @click=${(e) => {
              day.selected = true;
              if (!this.startDateSelected) {
                this.start = day;
                this.previousStartDate = e.target;
                this.end = moment(day).date() + 1;
              }
              if (!this.endDateSelected) {
                this.end = day;
                this.previousEndDate = e.target;
              }
              this.selectDate(day, e, daysInMonth);
            }}
            class=${classMap({
              current: moment(day).date() === this.date.getDate(),
              isBetween: day.between,
            })}
          >
            ${this.getDateLabel(day.date)}
            ${moment(day.date).date() === this.date.getDate() &&
            moment(day.date).month() === this.date.getMonth() &&
            moment(day.date).year() === this.date.getFullYear()
              ? html`
                  <div class="current-date"></div>
                `
              : ''}
          </li>
        `,
      );
    });
    return daysList;
  }

  firstUpdated() {
    this.nextDate.setMonth(this.date.getMonth() + 1);
    this.date.getMonth() == 11
      ? this.nextDate.setFullYear(this.date.getFullYear() + 1)
      : this.nextDate.setFullYear(this.date.getFullYear());
    this.nextEndDate = new Date();
    this.nextEndDate.setMonth(this.date.getMonth() + 1);
    this.date.getMonth() == 11
      ? this.nextEndDate.setFullYear(this.date.getFullYear() + 1)
      : this.nextEndDate.setFullYear(this.date.getFullYear());

    this.nextMonthStartDate = new Date(this.nextDate);
    this.nextMonthEndDate = new Date(this.nextEndDate);
    // this.buildCalendar(this.date);
    // this.buildCalendar(next);
    this.days = service.buildCalendar(this.date.getMonth(), this.date.getFullYear(), null, {
      startDate: this.selectedStartDate,
      endDate: this.selectedEndDate,
    });
    this.nextMonthDays = service.buildCalendar(
      this.date.getMonth() + 1,
      this.date.getFullYear(),
      null,
      { startDate: this.nextMonthStartDate, endDate: this.nextMonthEndDate },
    );
    this.currentDaysList = this.getDays(this.days);
    this.nextMonthDaysList = this.getDays(this.nextMonthDays);
  }

  changeMonth(monthToChange) {
    let changedMonthDays: Array<CalendarItem> = [];
    if (monthToChange == this.date) {
      changedMonthDays = service.buildCalendar(
        monthToChange.getMonth(),
        monthToChange.getFullYear(),
        null,
        { startDate: this.selectedStartDate, endDate: this.selectedEndDate },
      );
    } else {
      changedMonthDays = service.buildCalendar(
        monthToChange.getMonth(),
        monthToChange.getFullYear(),
        null,
        { startDate: this.nextMonthStartDate, endDate: this.nextMonthEndDate },
      );
    }

    return changedMonthDays;
  }

  selectDate(item, elem, daysInMonth) {
    if (item.selected) {
      elem.target.classList.add('selected');
    }
    if (item.disable) return;
    if (!this.startDateSelected) {
      this.startDateSelected = true;
      this.selectedStartDate = moment(item.date).format('YYYY-MM-DD');
    }
    if (item.date < this.start.date) {
      // this.startDateSelected = true;
      this.selectedStartDate = moment(item.date).format('YYYY-MM-DD');
      this.start = item;
      this.previousStartDate.classList.remove('selected');
    }
    if (item.date > this.start.date) {
      this.endDateSelected = true;
      this.selectedEndDate = moment(item.date).format('YYYY-MM-DD');
      this.end = item;
      this.previousEndDate.classList.remove('selected');

      if (
        !this.previousEndDate ||
        this.previousEndDate.innerHTML !==
          moment(item.date)
            .format('YYYY-MM-DD')
            .slice(8, 10)
      ) {
        this.previousEndDate = elem.target;
      }
    }
    if (this.endDateSelected) {
      // if (this.start.date.month()!==this.end.date.month()) {
      //   this.nextMonthDays.map((day) => {
      //     if (day.date > this.start.date && day.date < this.end.date) {
      //       this.nextMonthDays[this.nextMonthDays.indexOf(day)].between = true;
      //     } else {
      //       this.nextMonthDays[this.nextMonthDays.indexOf(day)].between = false;
      //     }
      //   });
      //   this.nextMonthDaysList=this.getDays(this.nextMonthDays);
      //   console.log(this.nextMonthDaysList);
      // }
      daysInMonth.map((day) => {
        if (day.date > this.start.date && day.date < this.end.date) {
          this.days[this.days.indexOf(day)].between = true;
        } else {
          this.days[this.days.indexOf(day)].between = false;
        }
      });
      this.showCalendar = false;
      this.currentDaysList=this.getDays(daysInMonth);
    }
    this.requestUpdate();
  }

  getDateLabel(item) {
    if (item === ' ') {
      return ' ';
    }
    return moment(item).format('DD');
  }

  getMonthName(date) {
    const monthName = new Intl.DateTimeFormat('en-US', { month: 'long' }).format;
    const longName = monthName(date);
    return longName;
  }
  _toggleCalendar() {
    this.showCalendar = !this.showCalendar;
    this.requestUpdate();
  }

  render() {
    return html`
      <div style="display:flex">
        <div
          class="datepicker-wrapper"
          @click=${this._toggleCalendar}
          @blur=${() => {
            this.showCalendar = false;
            this.requestUpdate();
          }}
        >
          <input
            type="text"
            class="input-date"
            placeholder="dd/mm/yyyy"
            .value=${this.selectedStartDate}
          />
          <svg
            focusable="false"
            preserveAspectRatio="xMidYMid meet"
            style="will-change: transform;"
            xmlns="http://www.w3.org/2000/svg"
            data-date-picker-icon="true"
            class="bx--date-picker__icon"
            width="20"
            height="24"
            viewBox="0 0 14 14"
            aria-hidden="true"
          >
            <path
              d="M13,2h-2V1h-1v1H6V1H5v1H3C2.4,2,2,2.4,2,3v10c0,0.6,0.4,1,1,1h10c0.6,0,1-0.4,1-1V3C14,2.4,13.6,2,13,2z M13,13H3V6h10V13z M13,5H3V3h2v1h1V3h4v1h1V3h2V5z"
            ></path>
          </svg>
        </div>
        <div
          class="datepicker-wrapper"
          @click=${this._toggleCalendar}
          @blur=${() => {
            this.showCalendar = false;
            this.requestUpdate();
          }}
        >
          <input
            type="text"
            class="input-date"
            placeholder="dd/mm/yyyy"
            .value=${this.selectedEndDate}
          />
          <svg
            focusable="false"
            preserveAspectRatio="xMidYMid meet"
            style="will-change: transform;"
            xmlns="http://www.w3.org/2000/svg"
            data-date-picker-icon="true"
            class="bx--date-picker__icon"
            width="20"
            height="24"
            viewBox="0 0 14 14"
            aria-hidden="true"
          >
            <path
              d="M13,2h-2V1h-1v1H6V1H5v1H3C2.4,2,2,2.4,2,3v10c0,0.6,0.4,1,1,1h10c0.6,0,1-0.4,1-1V3C14,2.4,13.6,2,13,2z M13,13H3V6h10V13z M13,5H3V3h2v1h1V3h4v1h1V3h2V5z"
            ></path>
          </svg>
        </div>
      </div>
      <div
        class=${classMap({
          'hide-calendar': !this.showCalendar,
          'calendar': true,
        })}
      >
        <div style="display:flex">
          <div
            class=${classMap({
              // 'hide-calendar': !this.showCalendar,
              prevMonth: true,
            })}
          >
            <ul class="weekdays">
              <li>S</li>
              <li>M</li>
              <li>T</li>
              <li>W</li>
              <li>T</li>
              <li>F</li>
              <li>S</li>
            </ul>
            <div style="display:flex;justify-content:center;padding: 0 5px 0 5px">
              <span
                @click=${() => {
                  // this.nextDate.setMonth(this.date.getMonth());
                  this.nextMonthDays = this.changeMonth(this.date);
                  this.nextMonthDaysList = this.getDays(this.nextMonthDays);
                  this.date.setMonth(this.date.getMonth() - 1);
                  if (this.date.getMonth() < 0) {
                    this.date.setFullYear(this.date.getFullYear() - 1);
                  }
                  this.days = this.changeMonth(this.date);
                  this.currentDaysList = this.getDays(this.days);
                  this.requestUpdate();
                }}
                style="flex:auto;cursor:pointer"
                >&#10094;</span
              >
              <span style="flex:auto"
                >${this.getMonthName(this.date)} ${this.date.getFullYear()}</span
              >
            </div>
            <ul class="days" role="group">
              ${this.currentDaysList}
            </ul>
          </div>

          <div
            class=${classMap({
              nextMonth: true,
            })}
          >
            <ul class="weekdays">
              <li>S</li>
              <li>M</li>
              <li>T</li>
              <li>W</li>
              <li>T</li>
              <li>F</li>
              <li>S</li>
            </ul>
            <div style="display:flex;justify-content:center;padding: 0 5px 0 5px">
              <span>${this.getMonthName(this.nextDate)} ${this.nextDate.getFullYear()}</span>
              <span
                @click=${() => {
                  // this.date.setMonth(this.nextDate.getMonth()-1);
                  this.days = this.changeMonth(this.nextDate);
                  this.currentDaysList = this.getDays(this.days);
                  this.nextDate.setMonth(this.nextDate.getMonth() + 1);
                  if (this.nextDate.getMonth() > 12) {
                    this.nextDate.setFullYear(this.nextDate.getFullYear() + 1);
                  }
                  this.nextMonthDays = this.changeMonth(this.nextDate);
                  this.nextMonthDaysList = this.getDays(this.nextMonthDays);
                  this.requestUpdate();
                }}
                style="cursor:pointer;position:relative;right:-80px"
                >&#10095;</span
              >
            </div>
            <ul class="days" role="group">
              ${this.nextMonthDaysList}
            </ul>
          </div>
        </div>
      </div>
    `;
  }
}
